package com.lasapps.simplenote;

import java.util.Random;

public class Utils {

    public static final String CHANNEL_ID = "MYID";
    public static final String CHANNEL_NAME = "MYCHANNEL_NAME";
    public static final String CHANNEL_DESC = "MYCHANNEL_DESC";
    public static final int NOTIFICATION_ID = 11;


    public static int generateRandomNumber() {
        int min = 0;
        int max = 999999999;

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
